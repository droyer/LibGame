﻿using LibExec;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class SceneGame : Scene
    {
        [Sync]
        int syncTest;

        [Sync]
        List<string> stringList;

        PauseMenu pauseMenu;

        public SceneGame()
        {
            bReplicate = true;
            Screen.SetScale(1.5f);
        }

        void Start()
        {
            if (Network.IsServer() || !Network.IsValid())
                SpawnActor<Entity>();

            stringList = new List<string>();
            if (Network.IsListenServer())
                SpawnActorWithServerAuthority<Player>(new Vector2(75, 75));

            if (!Network.IsValid())
                SpawnActor<Player>(new Vector2(75, 75));

            if (!Network.IsServerOnly())
            {
                pauseMenu = SpawnActor<PauseMenu>();
                pauseMenu.SetActive(false);
            }
        }

        void Update()
        {
            if (Input.GetKey(Keys.P))
                RPC("ServerIncreaseVar", 1);
            if (Input.GetKeyDown(Keys.T))
            {
                string file = "text.txt";
                if (File.Exists(file))
                {
                    StreamReader sr = new StreamReader(file, Encoding.Default);
                    string text = sr.ReadLine();
                    if (text != null && text != "")
                        RPC("ServerNewText", text);
                    sr.Close();
                }
            }

            if (Input.GetKeyDown(Keys.Escape) && Player.local != null)
            {
                pauseMenu.SetActive(!pauseMenu.bActive);
                Player.local.SetUpdatable(!pauseMenu.bActive);
                Player.local.StopFire();
            }
        }

        void Draw()
        {
            spriteBatch.Draw(Resources.Get<Texture2D>(Data.Screen), Vector2.Zero, Color.White);
        }

        void Draw2D()
        {
            spriteBatch.DrawString(Resources.Get<SpriteFont>(Data.Font), "Game scene: " + syncTest, Vector2.Zero, Color.Black);

            for (int i = 0; i < stringList.Count; i++)
                spriteBatch.DrawString(Resources.Get<SpriteFont>(Data.Font), stringList[i], new Vector2(0, 50 + i * 30), Color.Black);
        }

        [Server]
        void ServerIncreaseVar(int delta)
        {
            syncTest += delta;
        }

        [Server]
        void ServerNewText(string text)
        {
            stringList.Add(text);
        }

        void OnPostLogin(NetPlayer netPlayer)
        {
            SpawnActorWithAuthority<Player>(netPlayer, new Vector2(150, 150));
        }

        void OnDisconnected()
        {
            Load<SceneMenu>();
        }
    }
}
