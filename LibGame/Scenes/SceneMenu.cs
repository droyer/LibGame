﻿using LibExec;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class SceneMenu : Scene
    {
        SpriteFont font;

        string[] buttons = new string[]
        {
            "Play solo",
            "Host and play",
            "Connect",
            "Quit"
        };

        int selectButton;

        bool bConnecting;

        void Start()
        {
            font = Resources.Get<SpriteFont>(Data.Font);
        }

        void Update()
        {
            if (!bConnecting)
            {
                if (Input.GetKeyDown(Keys.S) || Input.GetKeyDown(Keys.Down))
                    selectButton++;
                if (Input.GetKeyDown(Keys.W) || Input.GetKeyDown(Keys.Up))
                    selectButton--;
                if (selectButton < 0)
                    selectButton = buttons.Length - 1;
                if (selectButton >= buttons.Length)
                    selectButton = 0;

                if (Input.GetKeyDown(Keys.Enter))
                {
                    if (selectButton == 0)
                        Load<SceneGame>();
                    else if (selectButton == 1)
                    {
                        if (Network.ListenServer(7777))
                            Load<SceneGame>();
                    }
                    else if (selectButton == 2)
                    {
                        Network.Connect("127.0.0.1", 7777);
                        bConnecting = true;
                    }
                    else if (selectButton == 3)
                        GWindow.ExitGame();
                }
            }

            if (Input.GetKeyDown(Keys.Escape))
                GWindow.ExitGame();
        }

        void Draw2D()
        {
            spriteBatch.Draw(Resources.whiteTexture, Screen.rectangle, new Color(50, 50, 50));

            spriteBatch.DrawStringCenter(font, "LibGame", new Vector2(Screen.center.X, 50), Color.White);

            if (!bConnecting)
            {
                for (int i = 0; i < buttons.Length; i++)
                {
                    Color buttonColor = Color.White;
                    if (selectButton == i)
                        buttonColor = new Color(150, 50, 50);
                    spriteBatch.DrawStringCenter(font, buttons[i], new Vector2(Screen.center.X, 200 + i * font.LineSpacing), buttonColor);
                }
            }
            else
            {
                spriteBatch.DrawStringCenter(font, "Connecting...", Screen.center, Color.White);
            }
        }
    }
}
