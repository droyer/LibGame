﻿using LibExec;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class Player : Actor
    {
        public static Player local;

        float speed;

        Vector2 velocity;

        [Sync(Condition.OwnerOnly)]
        Weapon weapon;

        public Player()
        {
            bReplicate = true;
            bReplicateMovement = true;
            bReplicateRotation = true;
            bLocalPlayer = true;

            layer = 1;

            speed = 500;

            if (!Network.IsServerOnly())
            {
                texture = Resources.Get<Texture2D>(Data.Player);
                width = texture.Width;
                height = texture.Height;
                origin = new Vector2(width / 2, height / 2);
            }
        }

        void Start()
        {
            if (Network.IsServer() || !Network.IsValid())
            {
                weapon = SpawnActor<Weapon>(this);
                AddChild(weapon);
            }

            if (IsLocalPlayer())
            {
                local = this;
                Screen.SetView(this);
            }
        }

        void Update()
        {
            if (IsLocalPlayer())
            {
                velocity = Vector2.Zero;

                if (Input.GetKey(Keys.D))
                    velocity.X = 1;
                if (Input.GetKey(Keys.A))
                    velocity.X = -1;
                if (Input.GetKey(Keys.W))
                    velocity.Y = -1;
                if (Input.GetKey(Keys.S))
                    velocity.Y = 1;

                if (velocity != Vector2.Zero)
                {
                    velocity.Normalize();
                    Move(velocity * Time.deltaTime * speed);
                }

                if (weapon != null)
                {
                    if (Input.GetMouseButtonDown(MouseButton.Left))
                        StartFire();
                    if (Input.GetMouseButtonUp(MouseButton.Left))
                        StopFire();
                    if (Input.GetMouseButton(MouseButton.Right))
                        weapon.PutProjectile();
                }

                if (Input.GetKeyDown(Keys.Q))
                    RPC("ServerDestroy");

                LookAt(Screen.GetWorldPosition(Input.GetMousePosition()));
            }
        }

        [Server]
        void ServerDestroy()
        {
            Destroy();
        }

        void StartFire()
        {
            if (weapon != null)
                weapon.StartFire();
        }

        public void StopFire()
        {
            if (weapon != null)
                weapon.StopFire();
        }
    }
}
