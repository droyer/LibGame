﻿using LibExec;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public struct WeaponHit
    {
        public Point firePosition;
        byte ensureReplicationByte;

        public void EnsureReplication()
        {
            ensureReplicationByte++;
        }
    }

    public class Weapon : Actor
    {
        Player player;

        [Sync(Condition.SkipOwner, hook = "OnFire")]
        WeaponHit hit;

        TimerHandle fireTimer;
        float fireRate = 0.1f;
        float lastFireTime;

        public Weapon()
        {
            bReplicate = true;
            sendRate = 30;
            SetUpdatable(false);
            SetDrawable(false);
        }

        void Start()
        {
            player = owner as Player;
        }

        public void StartFire()
        {
            float firstDelay = Math.Max(lastFireTime + fireRate - Time.time, 0.0f);

            fireTimer = Timer.Set(Fire, this, fireRate, true, firstDelay);
        }

        public void StopFire()
        {
            Timer.Clear(fireTimer);
        }

        void Fire()
        {
            Vector2 mousePosition = Input.GetMousePosition() + player.position - Screen.center;

            FireProjectile(mousePosition);

            RPC("ServerFire", mousePosition.ToPoint());

            lastFireTime = Time.time;
        }

        [Server]
        void ServerFire(Point firePosition)
        {
            hit.firePosition = firePosition;
            hit.EnsureReplication();

            if (!player.IsLocalPlayer())
                OnFire();
        }

        void OnFire()
        {
            FireProjectile(hit.firePosition.ToVector2());
        }

        void FireProjectile(Vector2 firePosition)
        {
            Projectile projectile = SpawnActor<Projectile>(player.position);
            Vector2 dir = firePosition - player.position;
            dir.Normalize();
            projectile.SetVelocity(dir);
        }

        public void PutProjectile()
        {
            RPC("ServerPutProjectile", player.position.ToPoint());
        }

        [Server]
        void ServerPutProjectile(Point position)
        {
            RPC("MulticastPutProjectile", position);
        }

        [Multicast]
        void MulticastPutProjectile(Point position)
        {
            SpawnActor<Projectile>(position.ToVector2());
        }
    }
}
