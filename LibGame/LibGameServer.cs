﻿using LibExec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibGame
{
    public class LibGameServer : GServer
    {
        void Start()
        {
            Network.Host(7777);
            Scene.Load<SceneGame>();
        }
    }
}
