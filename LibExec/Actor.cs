﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public abstract class Actor : Behaviour
    {
        protected Texture2D texture;
        protected int width, height;
        protected Vector2 origin;
        protected Color color;

        internal bool bIsLocalPlayer;
        public bool bLocalPlayer { get; protected set; }
        public int layer { get; protected set; }

        internal TimerHandle lifeSpanTimer;
        internal bool bPendingKill;
        internal bool bPendingKillUpdate;
        internal bool bPendingKillDraw;

        public Actor owner { get; internal set; }
        internal List<Actor> childActors;

        public Vector2 position { get; internal set; }
        internal Vector2 targetPosition;
        public float rotation { get; internal set; }
        internal float targetRotation;

        public bool bReplicateMovement { get; protected set; }
        public bool bReplicateRotation { get; protected set; }

        float sendRateMovementMs;
        Vector2 oldPosition;
        float oldRotation;

        public bool bActive { get; internal set; }
        public bool bUpdatable { get; internal set; }
        public bool bDrawable { get; internal set; }

        public Actor()
        {
            sequenceChannel = 2;
            sendRateMovementMs = 1.0f / 30.0f;
            childActors = new List<Actor>();
            bActive = true;
            bUpdatable = true;
            bDrawable = true;

            color = Color.White;
        }

        internal override void DoStart()
        {
            base.DoStart();

            targetPosition = position;
            targetRotation = rotation;

            if (HasAuthority() && bReplicate && Network.IsValid())
            {
                if (bReplicateMovement)
                    Timer.Set(SyncPosition, this, sendRateMovementMs, true);

                if (bReplicateRotation)
                    Timer.Set(SyncRotation, this, sendRateMovementMs, true);
            }

            if (startMethod != null)
                startMethod.Invoke(this, null);
        }

        internal override void DoUpdate()
        {
            base.DoUpdate();

            if (!HasAuthority() && bReplicate)
            {
                if (bReplicateMovement)
                    position = Vector2.SmoothStep(position, targetPosition, Time.deltaTime * 30);
                if (bReplicateRotation)
                    rotation = Mathf.SmoothStepAngle(rotation, targetRotation, Time.deltaTime * 30);
            }

            if (updateMethod != null)
                updateMethod.Invoke(this, null);
        }

        internal override void DoDraw()
        {
            base.DoDraw();

            Draw();
        }

        protected virtual void Draw()
        {
            if (texture != null)
                spriteBatch.Draw(texture, position, null, color, rotation, origin, 1, SpriteEffects.None, 0);
            else
            {
                Rectangle rectangle = new Rectangle((int)position.X, (int)position.Y, width, height);
                spriteBatch.Draw(Resources.whiteTexture, rectangle, rectangle, color, rotation, origin, SpriteEffects.None, 0);
            }
        }

        internal override void DoDraw2D()
        {
            base.DoDraw2D();

            if (draw2DMethod != null)
                draw2DMethod.Invoke(this, null);
        }

        public void SetParent(Actor parent)
        {
            if (Check(parent))
                parent.childActors.Add(this);
        }

        public void AddChild(Actor child)
        {
            if (Check(child))
                childActors.Add(child);
        }

        public bool IsLocalPlayer()
        {
            if (!Network.IsValid())
                return true;

            return bIsLocalPlayer;
        }

        public void Destroy()
        {
            if (bReplicate && !Network.IsServer() && Network.IsValid())
                return;
            if (bPendingKill)
                return;

            bPendingKill = true;

            foreach (var item in childActors)
                item.Destroy();

            if (bReplicate && Network.IsValid())
            {
                Scene.networkActors.Remove(id);

                NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.Destroy);
                outmsg.Write(this);
                Network.server.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered);
            }
            else
                Scene.actors.Remove(id);
        }

        public bool IsPendingKill()
        {
            return bPendingKill;
        }

        public void SetLifeSpan(float lifeSpan)
        {
            if (bReplicate && !Network.IsServer() && Network.IsValid())
                return;

            if (lifeSpan >= 0)
                Timer.Clear(lifeSpanTimer);
            if (lifeSpan > 0)
                lifeSpanTimer = Timer.Set(Destroy, this, lifeSpan, false); 
        }

        public void Move(Vector2 velocity)
        {
            SetPosition(position + velocity);
        }

        public void SetPosition(Vector2 position)
        {
            if (!HasAuthority() && bReplicateMovement)
                return;

            this.position = position;
        }

        public void Rotate(float degrees)
        {
            float angle = MathHelper.ToRadians(degrees);
            SetRotation(rotation + angle);
        }

        public void LookAt(Vector2 lookPosition)
        {
            Vector2 direction = lookPosition - position;
            direction.Normalize();
            SetRotation(Mathf.Atan2(direction.Y, direction.X));
        }

        public void SetRotation(float rotation)
        {
            if (!HasAuthority() && bReplicateRotation)
                return;

            this.rotation = MathHelper.WrapAngle(rotation);
        }

        void SyncPosition()
        {
            if (position == oldPosition)
                return;
            oldPosition = position;

            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.SyncPosition);
            outmsg.Write(this);
            outmsg.Write(position);
            SendMessage(outmsg);
        }

        void SyncRotation()
        {
            if (rotation == oldRotation)
                return;
            oldRotation = rotation;

            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.SyncRotation);
            outmsg.Write(this);
            outmsg.Write(rotation);
            SendMessage(outmsg);
        }

        void SendMessage(NetOutgoingMessage outmsg)
        {
            if (Network.IsServer())
                Network.server.SendToAll(outmsg, null, NetDeliveryMethod.ReliableSequenced, sequenceChannel);
            else
                Network.client.SendMessage(outmsg, NetDeliveryMethod.ReliableSequenced, sequenceChannel);
        }

        public bool HasAuthority()
        {
            if (!Network.IsValid() || !bReplicate)
                return true;

            if ((bLocalPlayer && IsLocalPlayer()) || (!bLocalPlayer && Network.IsServer()))
                return true;

            return false;
        }

        public void SetUpdatable(bool state)
        {
            if (bUpdatable == state)
                return;

            bUpdatable = state;
            if (bUpdatable && bPendingKillUpdate)
            {
                bPendingKillUpdate = false;
                return;
            }
            if (!bUpdatable)
                bPendingKillUpdate = true;
            else
                Scene.updatableActors.Add(this);
        }

        public void SetDrawable(bool state)
        {
            if (bDrawable == state)
                return;

            bDrawable = state;
            if (bDrawable && bPendingKillDraw)
            {
                bPendingKillDraw = false;
                return;
            }
            if (!bDrawable)
                bPendingKillDraw = true;
            else
                Scene.drawableActors.Add(layer, this);
        }

        public void SetActive(bool state)
        {
            if (bActive == state)
                return;

            bActive = state;
            if (!state)
            {
                bPendingKillUpdate = true;
                bPendingKillDraw = true;
            }
            else
            {
                bool bReturn = false;
                if (bUpdatable && bPendingKillUpdate)
                {
                    bPendingKillUpdate = false;
                    bReturn = true;
                }
                if (bDrawable && bPendingKillDraw)
                {
                    bPendingKillDraw = false;
                    bReturn = true;
                }
                if (bReturn)
                    return;

                if (bUpdatable)
                    Scene.updatableActors.Add(this);
                if (bDrawable)
                    Scene.drawableActors.Add(layer, this);
            }
        }
    }
}
