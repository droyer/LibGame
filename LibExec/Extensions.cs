﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public static class Extensions
    {
        public static void DrawStringCenter(this SpriteBatch spriteBatch, SpriteFont font, string text, Vector2 position, Color color)
        {
            Vector2 origin = new Vector2(font.MeasureString(text).X / 2, font.LineSpacing / 2);
            spriteBatch.DrawString(font, text, position, color, 0f, origin, 1, SpriteEffects.None, 0);
        }
    }
}
