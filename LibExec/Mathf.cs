﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public static class Mathf
    {
        public static float Atan2(float y, float x)
        {
            return (float)Math.Atan2(y, x);
        }

        public static float SmoothStepAngle(float nowrap, float wraps, float lerp)
        {
            float c, d;

            if (wraps < nowrap)
            {
                c = wraps + MathHelper.TwoPi;
                //c > nowrap > wraps
                d = c - nowrap > nowrap - wraps
                    ? MathHelper.SmoothStep(nowrap, wraps, lerp)
                    : MathHelper.SmoothStep(nowrap, c, lerp);

            }
            else if (wraps > nowrap)
            {
                c = wraps - MathHelper.TwoPi;
                //wraps > nowrap > c
                d = wraps - nowrap > nowrap - c
                    ? MathHelper.SmoothStep(nowrap, c, lerp)
                    : MathHelper.SmoothStep(nowrap, wraps, lerp);

            }
            else { return nowrap; } //Same angle already

            return MathHelper.WrapAngle(d);
        }
    }
}
