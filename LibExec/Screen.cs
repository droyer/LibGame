﻿using Microsoft.Xna.Framework;

namespace LibExec
{
    public static class Screen
    {
        public static int width { get; private set; }
        public static int height { get; private set; }
        public static Vector2 center { get; private set; }
        public static Rectangle rectangle { get; private set; }

        internal static Actor viewActor;
        internal static float viewScale;
        internal static Vector2 viewPosition;

        public static void SetSize(int width, int height)
        {
            Screen.width = width;
            Screen.height = height;
            center = new Vector2(width / 2, height / 2);
            rectangle = new Rectangle(0, 0, width, height);
        }

        public static void SetScale(float viewScale)
        {
            Screen.viewScale = viewScale;
        }

        public static void SetView(Actor viewActor)
        {
            Screen.viewActor = viewActor;
        }

        public static void SetView(Vector2 viewPosition)
        {
            Screen.viewPosition = viewPosition;
            Screen.viewActor = null;
        }

        internal static Matrix GetTransform()
        {
            if (Scene.Check(viewActor))
            {
                return Matrix.CreateTranslation(new Vector3(-viewActor.position + Screen.center / viewScale, 0)) *
                   Matrix.CreateScale(viewScale);
            }

            return Matrix.CreateTranslation(new Vector3(-viewPosition + Screen.center / viewScale, 0)) *
                   Matrix.CreateScale(viewScale);
        }

        public static Vector2 GetWorldPosition(Vector2 position)
        {
            Vector2 pos = viewPosition;
            if (Scene.Check(viewActor))
                pos = viewActor.position;

            return position + pos - Screen.center;
        }
    }
}
