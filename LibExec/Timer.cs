﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace LibExec
{
    public sealed class TimerHandle
    {
        float time;
        float targetTime;
        Action action;
        bool bLoop;
        float firstDelay;
        internal readonly Behaviour instance;

        float timer;

        internal bool bPendingKill;

        internal TimerHandle(Action action, Behaviour instance, float time, bool bLoop, float firstDelay = -1)
        {
            this.action = action;
            this.instance = instance;
            this.time = time;
            this.bLoop = bLoop;
            this.firstDelay = firstDelay;

            if (firstDelay == -1)
                targetTime = time;
            else
                targetTime = firstDelay;
        }

        internal void Update()
        {
            timer += Time.deltaTime;

            if (timer >= targetTime)
            {
                action();
                targetTime = time;
                if (!bLoop)
                    bPendingKill = true;
                timer = 0;
            }
        }
    }

    public static class Timer
    {
        internal static List<TimerHandle> timerList = new List<TimerHandle>();

        internal static void Update()
        {
            for (int i = 0; i < timerList.Count; i++)
            {
                TimerHandle timer = timerList[i];

                if (timer.bPendingKill || !Scene.Check(timer.instance))
                {
                    timerList.RemoveAt(i);
                    i--;
                }
                else
                    timer.Update();
            }
        }

        public static TimerHandle Set(Action action, Behaviour instance, float targetTime, bool bLoop, float firstDelay = -1)
        {
            TimerHandle handle = new TimerHandle(action, instance, targetTime, bLoop, firstDelay);
            timerList.Add(handle);
            return handle;
        }

        public static void Clear(TimerHandle timer)
        {
            if (timer != null)
                timer.bPendingKill = true;
        }

        public static bool IsValid(TimerHandle timer)
        {
            return (timer != null && !timer.bPendingKill);
        }
    }
}
