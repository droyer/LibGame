﻿using Lidgren.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    internal sealed class SyncInfo
    {
        public readonly Behaviour instance;
        public readonly byte index;
        readonly FieldInfo field;
        readonly MethodInfo method;
        public readonly Sync sync;

        readonly bool bList;
        int oldCount;

        object oldValue;

        public SyncInfo(Behaviour instance, byte index, FieldInfo field, Sync sync)
        {
            this.instance = instance;
            this.index = index;
            this.field = field;
            this.sync = sync;
            if (sync.hook != null)
                method = instance.GetType().GetMethod(sync.hook, BindingFlags.Instance | BindingFlags.NonPublic);

            bList = GetFieldType().Name.Contains("List");
            UpdateOldValue(field.GetValue(instance));
        }

        public object GetValue()
        {
            object value = field.GetValue(instance);
            return value;
        }

        public void SetValue(object value, bool bHookEvent)
        {
            field.SetValue(instance, value);
            if (method != null && bHookEvent)
                method.Invoke(instance, null);
        }

        public Type GetFieldType()
        {
            return field.FieldType;
        }

        public bool NeedUpdate()
        {
            object value = field.GetValue(instance);
            bool state;

            if (bList)
                state = oldCount != ((IList)value).Count;
            else
                state = !value.Equals(oldValue);
            UpdateOldValue(value);
            return state;
        }

        void UpdateOldValue(object value)
        {
            oldValue = value;
            if (bList && value != null)
                oldCount = ((IList)value).Count;
        }
    }

    public enum Condition
    {
        NoCondition,
        SkipOwner,
        OwnerOnly
    }

    public sealed class Sync : Attribute
    {
        public string hook;
        Condition condition;

        public Sync()
        {
            condition = Condition.NoCondition;
        }

        public Sync(Condition condition)
        {
            this.condition = condition;
        }

        internal static void Update(Behaviour instance)
        {
            if (!Scene.Check(instance))
                return;

            foreach (var info in instance.syncList)
            {
                if (info.GetValue() == null)
                    continue;

                if (info.NeedUpdate())
                {
                    if (info.sync.condition == Condition.OwnerOnly)
                    {
                        if (instance.ownerConnection == null)
                            continue;

                        Send(NetworkData.Sync, info, instance.ownerConnection, NetDeliveryMethod.ReliableSequenced);
                    }
                    else
                    {
                        foreach (var connection in Network.connections)
                        {
                            if (info.sync.condition == Condition.SkipOwner && instance.ownerConnection == connection)
                                continue;

                            Send(NetworkData.Sync, info, connection, NetDeliveryMethod.ReliableSequenced);
                        }
                    } 
                }
            }
        }

        internal static void UpdateForConnection(Behaviour instance, NetConnection connection)
        {
            foreach (var info in instance.syncList)
            {
                if (info.GetValue() == null)
                    continue;

                if (info.sync.condition == Condition.SkipOwner && instance.ownerConnection == connection)
                    continue;
                else if (info.sync.condition == Condition.OwnerOnly && instance.ownerConnection != connection)
                    continue;

                Send(NetworkData.SyncWithNoHookEvent, info, connection, NetDeliveryMethod.ReliableOrdered);
            }
        }

        internal static void Send(NetworkData data, SyncInfo info, NetConnection connection, NetDeliveryMethod method)
        {
            NetOutgoingMessage outmsg = Network.CreateMessage(data);
            outmsg.Write(info.instance);
            outmsg.Write(info);
            outmsg.WriteObject(info.GetValue());

            Network.server.SendMessage(outmsg, connection, method, info.instance.sequenceChannel);
        }

        internal static List<SyncInfo> CreateList(Behaviour instance)
        {
            List<SyncInfo> list = new List<SyncInfo>();
            FieldInfo[] infos = instance.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var info in infos)
            {
                Sync sync = info.GetCustomAttribute<Sync>();
                if (sync != null)
                    list.Add(new SyncInfo(instance, (byte)list.Count, info, sync));
            }
            return list;
        }
    }
}
