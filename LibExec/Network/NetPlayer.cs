﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public sealed class NetPlayer
    {
        internal static byte countID = 0;

        internal readonly NetConnection connection;
        internal readonly byte index;

        internal NetPlayer(NetConnection connection, byte index)
        {
            this.connection = connection;
            this.index = index;
        }
    }
}
