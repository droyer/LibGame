﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    internal static partial class NetExtensions
    {
        public static void Write(this NetBuffer buffer, Behaviour instance)
        {
            if (instance != null)
            {
                if (!instance.bReplicate)
                    throw new Exception("The class is not replicated !");
                buffer.Write(instance.id);
            }
            else
                buffer.Write(-1);
        }

        public static void WriteNewActor(this NetBuffer buffer, Actor instance)
        {
            buffer.Write(instance.GetType());
            buffer.Write(instance.id);
            buffer.Write(instance.owner);
            buffer.Write(instance.position);
            buffer.Write(instance.rotation);
        }

        public static void Write(this NetBuffer buffer, IList list)
        {
            int count = list.Count;
            buffer.Write(count);

            foreach (object obj in list)
                buffer.WriteObject(obj);
        }

        public static void Write(this NetBuffer buffer, Type type)
        {
            byte index = Network.types[type];
            buffer.Write(index);
        }

        public static void Write(this NetBuffer buffer, RPCInfo rpc)
        {
            buffer.Write(rpc.instance);
            buffer.Write(rpc.index);
        }

        public static void Write(this NetBuffer buffer, SyncInfo info)
        {
            buffer.Write(info.index);
        }

        public static void Write(this NetBuffer buffer, NetPlayer netPlayer)
        {
            buffer.Write(netPlayer.index);
        }

        public static void Write(this NetBuffer buffer, object[] args)
        {
            buffer.Write((byte)args.Length);
            for (int i = 0; i < args.Length; i++)
                buffer.WriteObject(args[i]);
        }

        public static void WriteStruct(this NetBuffer buffer, object value)
        {
            Type type = value.GetType();
            FieldInfo[] info = type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var item in info)
                buffer.WriteObject(item.GetValue(value));
        }

        public static void WriteArray(this NetBuffer buffer, object[] objs)
        {
            buffer.Write((byte)objs.Length);
            buffer.Write(objs[0].GetType());
            for (int i = 0; i < objs.Length; i++)
                buffer.WriteObject(objs[i]);
        }

        public static void WriteObject(this NetBuffer buffer, object obj)
        {
            if (obj is byte)
                buffer.Write((byte)obj);
            else if (obj is bool)
                buffer.Write((bool)obj);
            else if (obj is int)
                buffer.Write((int)obj);
            else if (obj is string)
                buffer.Write((string)obj);
            else if (obj is float)
                buffer.Write((float)obj);
            else if (obj is Vector2)
                buffer.Write((Vector2)obj);
            else if (obj is Point)
                buffer.Write((Point)obj);
            else if (obj is Actor)
                buffer.Write((Actor)obj);
            else if (obj is Array)
                buffer.WriteArray((object[])obj);
            else if (obj is IList)
                buffer.Write((IList)obj);
            else if (obj is NetPlayer)
                buffer.Write((NetPlayer)obj);
            else if (obj.GetType().IsValueType && !obj.GetType().IsEnum && !obj.GetType().IsPrimitive)
                buffer.WriteStruct(obj);
        }
    }
}
