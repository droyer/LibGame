﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LibExec
{
    public static partial class Network
    {
        static void UpdateServer()
        {
            while ((inc = Network.server.ReadMessage()) != null)
            {
                switch (inc.MessageType)
                {
                    case NetIncomingMessageType.ConnectionApproval:
                        if (Scene.current != null && Scene.current.bReplicate)
                            inc.SenderConnection.Approve();
                        else
                            inc.SenderConnection.Deny();
                        break;

                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)inc.ReadByte();
                        if (status == NetConnectionStatus.Connected)
                            ServerLogin();
                        else if (status == NetConnectionStatus.Disconnected)
                            Disconnected(inc.SenderConnection);
                        break;

                    case NetIncomingMessageType.Data:
                        NetworkData data = (NetworkData)inc.ReadByte();
                        if (data == NetworkData.Login) 
                            ServerLogin();
                        else if (data == NetworkData.PostLogin)
                            PostLogin();
                        else if (data == NetworkData.RPC)
                            RPC();
                        else if (data == NetworkData.SyncPosition)
                            ServerSyncPosition();
                        else if (data == NetworkData.SyncRotation)
                            ServerSyncRotation();
                        break;
                }
            }

            server.Recycle(inc);
        }

        static void Disconnected(NetConnection connection)
        {
            netPlayers.Remove(netPlayers.Single(t => t.connection == connection));

            connections.Remove(connections.Single(t => t == connection));

            for (int i = 0; i < Scene.networkActors.Count; i++)
            {
                var item = Scene.networkActors.ElementAt(i).Value;
                if (item.ownerConnection == connection)
                    item.Destroy();
            }
        }

        static void ServerLogin()
        {
            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.Login);

            NetPlayer netPlayer = new NetPlayer(inc.SenderConnection, NetPlayer.countID++);
            netPlayers.Add(netPlayer);
            outmsg.Write(netPlayer.index);

            outmsg.Write(Scene.current.GetType());

            outmsg.Write((short)(Scene.networkActors.Count));

            for (int i = 0; i < Scene.networkActors.Count; i++)
            {
                var item = Scene.networkActors.ElementAt(i);
                Actor instance = item.Value;
                outmsg.WriteNewActor(instance);
            }  

            Network.server.SendMessage(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableOrdered);
        }

        static void PostLogin()
        {
            Sync.UpdateForConnection(Scene.current, inc.SenderConnection);
            for (int i = 0; i < Scene.networkActors.Count; i++)
            {
                var item = Scene.networkActors.ElementAt(i);
                Actor obj = item.Value;
                Sync.UpdateForConnection(obj, inc.SenderConnection);
            }

            connections.Add(inc.SenderConnection);
            if (Scene.onPostLoginMethod != null)
                Scene.onPostLoginMethod.Invoke(Scene.current, new object[] { GetNetPlayer(inc.SenderConnection) });
        }

        static void ServerSyncPosition()
        {
            Actor instance = inc.ReadActor();
            if (instance == null)
                return;
            Vector2 position = inc.ReadVector2();
            instance.targetPosition = position;

            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.SyncPosition);
            outmsg.Write(instance);
            outmsg.Write(position);
            Network.server.SendToAll(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableSequenced, instance.sequenceChannel);
        }

        static void ServerSyncRotation()
        {
            Actor instance = inc.ReadActor();
            if (instance == null)
                return;
            float rotation = inc.ReadFloat();
            instance.targetRotation = rotation;

            NetOutgoingMessage outmsg = Network.CreateMessage(NetworkData.SyncRotation);
            outmsg.Write(instance);
            outmsg.Write(rotation);
            Network.server.SendToAll(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableSequenced, instance.sequenceChannel);
        }
    }
}
