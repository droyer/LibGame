﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibExec
{
    public static class Resources
    {
        static ContentManager content;

        public static Texture2D whiteTexture { get; private set; }

        static Dictionary<Enum, Texture2D> textures;

        static Dictionary<Enum, SpriteFont> fonts;

        internal static void Init(ContentManager content, GraphicsDevice graphicsDevice)
        {
            Resources.content = content;
            textures = new Dictionary<Enum, Texture2D>();
            fonts = new Dictionary<Enum, SpriteFont>();

            whiteTexture = new Texture2D(graphicsDevice, 1, 1);
            whiteTexture.SetData(new Color[] { Color.White });
        }

        public static void Set<T>(Enum data, string path)
        {
            if (typeof(T) == typeof(Texture2D))
                textures.Add(data, content.Load<Texture2D>(path));
            else if (typeof(T) == typeof(SpriteFont))
                fonts.Add(data, content.Load<SpriteFont>(path));
        }

        public static T Get<T>(Enum data) where T : class
        {
            object tex = null;
            if (typeof(T) == typeof(Texture2D))
                tex = textures[data];
            else if (typeof(T) == typeof(SpriteFont))
                tex = fonts[data];
            return (T)tex;
        }
    }
}
